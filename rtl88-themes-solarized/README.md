A GTK, xfwm4, openbox-3, and GNOME-Shell - Dark Mode Theme - Based on the Official Color Pallet Created by Ethan Schoonover.

Choose from the official Blue, Cyan, Green, Magenta, Orange, Red, and Violet flavors.


*** Solarized-Colors-gtk-theme-iconpack is now available, on this site in a separate package, to support Solarized-Dark-gtk-theme-colorpack. Please follow this link to check it out: https://www.opendesktop.org/p/1309239/


Brought back to the GUI by popular demand, Solarized-Dark - as originally created by Ethan Schoonover - is the highly valued color scheme for code editors and terminal emulators. Delivering GTK, xfwm4, openbox-3, and GNOME-Shell dark-mode-schemes, this GUI-centric theme strictly adheres to Ethan Schoonover's original color scheme, which has been published for many major applications, with some including the scheme pre-installed. Coders and GUI users, alike, know that - once you try Solarized-Dark - it is hard to go back to anything else.

In His Own Words:

"Solarized is a sixteen color palette (eight monotones, eight accent colors) designed for use with terminal and gui applications. It has several unique properties. I designed this colorscheme with both precise CIELAB lightness relationships and a refined set of hues based on fixed color wheel relationships. It has been tested extensively in real world use on color calibrated displays (as well as uncalibrated/intentionally miscalibrated displays) and in a variety of lighting conditions." https://ethanschoonover.com/solarized/

The GTK, xfwm4, openbox-3, and GNOME-Shell theme represents a careful blending of modernized Numix elements, in tandem with those of minimalist artist Rafa Capoci and full-stack web-developer/themer Pavel Agarkov, with an overlay of the original standard Solarized-Dark theming created by Ethan Schoonover.


Manual Installation Is Easy: 1) Extract the "tar.xz" file into your "~/.themes/" folder - to install for current user only - or into the "/usr/share/themes/" folder - for the theme to be applied globally. 2) Use the GNOME Tweak Tool or an equivalent app to enable it for your desktop.

Logging out and then logging back in may be necessary on some operating systems, to fully implement themes.

If installing manually, make sure to install the dependency: "Murrine theme engine" if you do not already have it; and update your GTK+packages if you have not.


Roboto and Roboto-Medium are the UI fonts, Midnight Lizard Solarized extension (for Chrome & FF) is the browser add-on shown in the screenshots. 
